import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    form: [
      {"id":1,"form_id":1,"parent_id":0,"type":"test","question":"\u041a\u0430\u043a \u0412\u0430\u0441 \u0437\u043e\u0432\u0443\u0442?","variant":"*","variants":[{"id":2,"form_id":1,"parent_id":1,"type":"input-radio","question":"\u0412\u0430\u043c \u0431\u043e\u043b\u044c\u0448\u0435 18 \u043b\u0435\u0442?","variant":"*","variants":[{"id":3,"form_id":1,"parent_id":2,"type":"text","question":"\u0412\u044b \u043d\u0430\u043c \u043d\u0435 \u043f\u043e\u0434\u0445\u043e\u0434\u0438\u0442\u0435","variant":"\u041d\u0435\u0442","variants":[]},{"id":4,"form_id":1,"parent_id":2,"type":"input-select","question":"\u0412\u044b \u043a\u0443\u0440\u0438\u0442\u0435?","variant":"\u0414\u0430","variants":[{"id":5,"form_id":1,"parent_id":4,"type":"text","question":"\u041a\u0443\u0440\u0435\u043d\u0438\u0435 \u0432\u0440\u0435\u0434\u0438\u0442 \u0437\u0434\u043e\u0440\u043e\u0432\u044c\u044e.","variant":"\u0414\u0430","variants":[{"id":7,"form_id":1,"parent_id":5,"type":"input-text","question":"\u0421\u043a\u043e\u043b\u044c\u043a\u043e \u0412\u0430\u043c \u043b\u0435\u0442?","variant":"*","variants":[]}]},{"id":6,"form_id":1,"parent_id":4,"type":"text","question":"\u0412\u044b \u043c\u043e\u043b\u043e\u0434\u0435\u0446","variant":"\u041d\u0435\u0442","variants":[{"id":8,"form_id":1,"parent_id":6,"type":"input-text","question":"\u0421\u043a\u043e\u043b\u044c\u043a\u043e \u0412\u0430\u043c \u043b\u0435\u0442?","variant":"*","variants":[]}]}]}]}]}
    ]
  },
  mutations: {
    updateForm(state, form) {
      state.form = form;
    }
  },
  actions: {},
  modules: {}
});
