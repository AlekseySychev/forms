import Vue from "vue";
import VueRouter from "vue-router";
import FormCreate from "../views/FormCreate.vue";
import FormView from "../views/FormView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "FormCreate",
    component: FormCreate
  },
  {
    path: "/view",
    name: "FormView",
    component: FormView
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
